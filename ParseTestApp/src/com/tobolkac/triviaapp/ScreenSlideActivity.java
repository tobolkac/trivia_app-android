/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tobolkac.triviaapp;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Demonstrates a "screen-slide" animation using a {@link ViewPager}. Because {@link ViewPager}
 * automatically plays such an animation when calling {@link ViewPager#setCurrentItem(int)}, there
 * isn't any animation-specific code in this sample.
 *
 * <p>This sample shows a "next" button that advances the user to the next step in a wizard,
 * animating the current screen out (to the left) and the next screen in (from the right). The
 * reverse animation is played when the user presses the "previous" button.</p>
 *
 * @see ScreenSlidePageFragment
 */
public class ScreenSlideActivity extends FragmentActivity {
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
   public static final int NUM_PAGES = 10;
   
   public int timeTaken;
  
   
   //new globals
   public boolean isChallenger;
   public String challengedUser;
   public String gameId;
   public String challenger;
   public String opponent;
   
   
   public int numCorrect;
   public int getNumCorrect()
   {
	   return numCorrect;
   }
   public void setNumCorrect(int val){
	   numCorrect = val;
   }
   
   public void incNumCorrect(){
	   numCorrect++;
   }
   
   public int getNumPages()
   {
	   return NUM_PAGES;
   }
   
   public int[] questionNums = {1,2,3,4,5,6,7,8,9,10};
   
   public String cat;
   
   public ParseObject q;
   
   List<ParseObject> questions;
   
   ParseObject gameScore;
   ParseUser currentUser;
   
    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    public static ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    public PagerAdapter mPagerAdapter;
    
    private long startTime = 0L;

	private Handler customHandler = new Handler();

	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;
	
	long millis;
	
	public long getTimeInMilliseconds()
	{
		return millis;
	}
	
	TextView timerTextView;
	
	Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {
            millis = System.currentTimeMillis() - startTime;
            int seconds = (int) (millis / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;

            timerTextView.setText(String.format("%d:%02d", minutes, seconds));

            timerHandler.postDelayed(this, 500);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide);
        Bundle b = getIntent().getExtras();
        Parse.initialize(this, "paHnFob0MGoBuy16Pzg5YPCH6TMOZfgZPXEOY1em", "1WOoBPDmOAu9CbHfvKIGmNIt2mY32mEvBYoLcPLV");
        ParseAnalytics.trackAppOpened(getIntent());
//        questionNums = b.getIntArray("questionsNumArray");
//        cat = b.getString("category");
        
        
        //Challenging (Creating Game)
        if(b.getString("com.parse.Data") == null && b.getString("gameId") == null)
        {
          isChallenger = true;
//          userList.setText("Challenging");
          challengedUser = getIntent().getStringExtra("opponentName");
          cat = b.getString("category");
          Log.d("category", "category: " + cat);
          //questionNums = b.getIntArray("questionNumArray");
          //getQuestions(questionNums);

        }
        else{
        //Being Challenged (From Homescreen)
	        if(b.getString("gameId") != null)
	        {
	        	gameId = b.getString("gameId");
	        }
	        else
	        {
	            try {
	      		  JSONObject data = new JSONObject(b.getString("com.parse.Data"));
	      		  gameId = data.getString("gameId");
	            } catch (JSONException e) {e.printStackTrace();}	
	        }
	        	isChallenger = false;
	        	
	        	  ParseQuery<ParseObject> query = ParseQuery.getQuery("Games");				  
	    		  try {
	    			ParseObject gameObj = query.get(gameId);
	    			challenger = gameObj.getString("challenger");
	    			opponent = gameObj.getString("opponent");
	    			cat = gameObj.getString("category");
//	    			List<Object> a = new ArrayList<Object>();
//	    			a = gameObj.getList("questionsArray");
//	    			int count =0;
//	    			questionNums = new int[a.size()];
//	    			for(Object o : a)
//	    			{
//	    				questionNums[count] = (Integer) o;
//	    				count++;
//	    			}
	    		} catch (ParseException e1) {e1.printStackTrace();}
	
        }
        
        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        FragmentManager manager = getSupportFragmentManager();
        mPagerAdapter = new ScreenSlidePagerAdapter(manager);
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return true;
			}
		});
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When changing pages, reset the action bar actions since they are dependent
                // on which page is currently active. An alternative approach is to have each
                // fragment expose actions itself (rather than the activity exposing actions),
                // but for simplicity, the activity provides the actions in this sample.
                invalidateOptionsMenu();
            }
        });
        
        timerTextView = (TextView) findViewById(R.id.gameTimer);
        
        startTime = System.currentTimeMillis();
        timerHandler.postDelayed(timerRunnable, 0);
        

        
        
    }
    

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_screen_slide, menu);

        //menu.findItem(R.id.action_previous).setEnabled(mPager.getCurrentItem() > 0);

        // Add either a "next" or "finish" button to the action bar, depending on which page
        // is currently selected.
        /*MenuItem item = menu.add(Menu.NONE, R.id.action_next, Menu.NONE,
                (mPager.getCurrentItem() == mPagerAdapter.getCount() - 1)
                        ? R.string.action_finish
                        : R.string.action_next);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Navigate "up" the demo structure to the launchpad activity.
                // See http://developer.android.com/design/patterns/navigation.html for more.
                NavUtils.navigateUpTo(this, new Intent(this, MainActivity.class));
                return true;

            /*case R.id.action_previous:
                // Go to the previous step in the wizard. If there is no previous step,
                // setCurrentItem will do nothing.
                mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                return true;*/

            /*case R.id.action_next:
                // Advance to the next step in the wizard. If there is no next step, setCurrentItem
                // will do nothing.
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                return true;*/
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A simple pager adapter that represents 5 {@link ScreenSlidePageFragment} objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

		@Override
        public Fragment getItem(int position) {
			Log.d("position", "position: "+position);
            return ScreenSlidePageFragment.create(position, cat);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
    
//    public void evalGame(int numCorrect, long time){
    public void evalGame(long time){
    	
    	if(isChallenger == true){

    		currentUser = ParseUser.getCurrentUser();
    		
	    	//Push some sample data into the database.
    		gameScore = new ParseObject("Games");
	        gameScore.put("challenger", currentUser.getUsername());
	        gameScore.put("opponent", challengedUser);
	        gameScore.put("challengerCorrect", numCorrect);
	        gameScore.put("winner", 0);
	        gameScore.put("challengerTime", time/1000);
	        gameScore.put("category", cat);
	        gameScore.put("numberOfQuestions", 10);
        	//gameScore.put("questionsArray", questionNums);
	      
	        gameScore.saveInBackground(new SaveCallback() {
	            public void done(ParseException e) {
	                if (e == null) {

	        	        String gameId = gameScore.getObjectId();
	                            
	                	
	        	        JSONObject data = new JSONObject();
	        	        try {
	        	        	data.put("game", "com.tobolkac.FETCH_GAME");
	        	        	data.put("challenger", currentUser.getUsername());
	        	        	data.put("opponent", challengedUser);
	        	        	data.put("gameId", gameId);	

	        			    } catch (JSONException e1) {e1.printStackTrace();}
	        	                
	        	            ParsePush androidPush = new ParsePush();
	        	            androidPush.setMessage(cat + " Trivia Challenge from " + currentUser.getUsername());
	        	            androidPush.setData(data);
	        	            androidPush.setChannel(challengedUser);
	        	            androidPush.sendInBackground();
	        	        
	        	        
	        	        
	        	            
	    					Intent intent = new Intent(ScreenSlideActivity.this, MainActivity.class);
	    					intent.putExtra("gameId", gameId);
	    					startActivity(intent);
	                }
	            }
	        });	 

    	}
    	else{
    		
    		
    		
    		
        	  ParseQuery<ParseObject> query = ParseQuery.getQuery("Games");	
        	  ParseQuery<ParseObject> queryChalllangerRecord = ParseQuery.getQuery("Record");	
        	  ParseQuery<ParseObject> queryOpponentRecord = ParseQuery.getQuery("Record");
    		  try {
    			ParseObject gameObj = query.get(gameId);
//    			int opponentTime = (int)(Math.random() * 100);
    			int challengerTime = gameObj.getNumber("challengerTime").intValue();
    			int challengerCorrect = gameObj.getNumber("challengerCorrect").intValue();    			
    			
    			gameObj.put("opponentTime", time/1000);
    			gameObj.put("opponentCorrect", numCorrect);
    			
    			queryChalllangerRecord.whereEqualTo("user", gameObj.getString("challanger"));
    			ParseObject challangerRecord = queryChalllangerRecord.getFirst();
    			
    			queryOpponentRecord.whereEqualTo("user", gameObj.getString("opponent"));
    			ParseObject opponentRecord = queryOpponentRecord.getFirst();
    			

    			
    			String res = "You won!";
    			//1 = challenger 2 = opponennt 0 = in progress
    			if(numCorrect > challengerCorrect){
    				//opponent wins
    				gameObj.put("winner", 2);
    				int oWins = opponentRecord.getInt("wins");
    				opponentRecord.put("wins", (oWins+1));
    				
    				int cLoses = challangerRecord.getInt("loses");
    				opponentRecord.put("wins", (cLoses+1));
    				
    			}
    			else if(numCorrect < challengerCorrect){
    				//challenger wins
    				gameObj.put("winner", 1);
    				res = "You suck!";
    				int oLoses = opponentRecord.getInt("loses");
    				opponentRecord.put("loses", (oLoses+1));
    				
    				int cWins = challangerRecord.getInt("wins");
    				challangerRecord.put("wins", (cWins+1));
    			}
    			else
    			{
    				if(time < challengerTime){
    					//opponent wins
        				gameObj.put("winner", 2);
        				int oWins = opponentRecord.getInt("wins");
        				opponentRecord.put("wins", (oWins+1));
        				
        				int cLoses = challangerRecord.getInt("loses");
        				challangerRecord.put("loses", (cLoses+1));
    				}
    				else{
    					//challenger wins
        				gameObj.put("winner", 1);
        				res = "You suck!";
        				int oLoses = opponentRecord.getInt("loses");
        				opponentRecord.put("loses", (oLoses+1));
        				
        				int cWins = challangerRecord.getInt("wins");
        				challangerRecord.put("wins", (cWins+1));
    				}
    			}

    			

				Intent intent = new Intent(ScreenSlideActivity.this, MainActivity.class);
				intent.putExtra("gameId",gameId);
				startActivity(intent);
				
				Toast.makeText(this, res, Toast.LENGTH_SHORT).show();
    			gameObj.saveInBackground();
    			opponentRecord.saveInBackground();
    			challangerRecord.saveInBackground();

    		} catch (ParseException e1) {e1.printStackTrace();}

    	}
    }
}
